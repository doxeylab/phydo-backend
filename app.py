#!flask/bin/python

import MySQLdb
from MySQLdb.constants import FIELD_TYPE
import sys, argparse
import re
from flask import Flask, jsonify,abort, make_response, request, url_for, current_app
from datetime import timedelta
from functools import update_wrapper
import tree_exporter
import config


app = Flask(__name__)

"""DB query related """
def getDb(username=config.username,password=config.password,
    host=config.host,
    database=config.database,
    port=config.port):
    _my_conv = { FIELD_TYPE.LONG: int,FIELD_TYPE.INT24:int}
    conn = MySQLdb.connect(user=username,passwd=password,host=host,db=database,conv=_my_conv, port=port)
    return conn

def getNodeIdFromTaxIds(db,taxids):
    str_taxids = ','.join(taxids)
    '''
    this is a rather lengthy way of getting genus ids from species ids (taxids)
    we check each ncbi taxonomy node matching `taxids` and including
    its ancestral nodes 3 levels up
    then we return all those nodes whose rank is 'genus'
    then take genus ids to look up node_genus table

    why do we do this?
    genus level is not the immediate parent of species level, it can go up 3 levels
    yes, ncbi taxonomy is messy
    '''
    sql = """
        SELECT node_id from tree_of_life.node_genus WHERE genus_id IN
        (
        SELECT t.ncbi_taxid
            FROM 
            (SELECT t.parent
                FROM
                (SELECT t.parent
                    FROM
                        (SELECT parent from
                            pfam.taxonomy WHERE ncbi_taxid IN (%s) AND (rank <> 'genus' OR rank is NULL)) AS x
                    join pfam.taxonomy t
                    ON t.ncbi_taxid = x.parent
                    WHERE (t.rank <> 'genus' OR t.rank IS NULL)
                ) AS x
                JOIN pfam.taxonomy t
                ON t.ncbi_taxid = x.parent
                WHERE (t.rank <> 'genus' OR t.rank IS NULL)
            ) AS x
            join pfam.taxonomy t
            ON t.ncbi_taxid = x.parent
            WHERE t.rank = 'genus'
        UNION ALL
        SELECT t.ncbi_taxid
            FROM
            (SELECT t.parent
                FROM
                    (SELECT parent from
                        pfam.taxonomy WHERE ncbi_taxid IN (%s) AND (rank <> 'genus' OR rank is NULL)) AS x
                join pfam.taxonomy t
                ON t.ncbi_taxid = x.parent
                WHERE (t.rank <> 'genus' OR t.rank IS NULL)
            ) AS x
            JOIN pfam.taxonomy t
            ON t.ncbi_taxid = x.parent
            WHERE t.rank = 'genus'
        UNION ALL
        SELECT t.ncbi_taxid
                FROM
                    (SELECT parent from
                        pfam.taxonomy WHERE ncbi_taxid IN (%s) AND (rank <> 'genus' OR rank is NULL)) AS x
                join pfam.taxonomy t
                ON t.ncbi_taxid = x.parent
                WHERE t.rank = 'genus'
        UNION ALL
        SELECT ncbi_taxid
            FROM pfam.taxonomy
            WHERE ncbi_taxid IN (%s) AND rank = 'genus'
        )
    """ % (str_taxids, str_taxids, str_taxids, str_taxids)
    cursor = db.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute(sql)
    rows = cursor.fetchall()
    node_ids = [row['node_id'] for row in rows]
    return node_ids

"""
returns something like 
[{
            'description': 'Flagellin N conserved domain',
            'pfamAcc': 'PF00669',
            'pfamId': 'Flg_N',
        } ...]
"""
def getArchitectureForSeq(db, seqId):
    sql = """
        SELECT a.description AS description, a.pfamA_acc AS pfamAcc, a.pfamA_id AS pfamId
        FROM pfam.pfamseq s 
        JOIN pfam.pfamA_reg_full_significant r ON s.pfamseq_acc = r.pfamseq_acc
        JOIN pfam.pfamA a ON r.pfamA_acc = a.pfamA_acc
        WHERE s.pfamseq_id = %s
        ORDER BY r.ali_start ASC;
    """
    c = db.cursor(MySQLdb.cursors.DictCursor)
    c.execute(sql,[seqId])
    rows = c.fetchall()
    return rows

"""
returns a nice, json looking hits array 
[{
    id: number,
    seqIds: [{
        seqId: <number>
        architecture: <getArchitectureForSeq array>
    }]
} ...]
"""
def getHits(db, rows):
    return [{
            'id': r['id'], 
            'seqIds': [{
                'seqId': seqId,
                'architecture': getArchitectureForSeq(db, seqId)
            } for seqId in r['seq_ids'].split(',')]
        } for r in rows if r['seq_ids']]

def getNodeIdFromArchitecture(db,depth,architecture):
    c = db.cursor(MySQLdb.cursors.DictCursor)
    c.execute("""CALL select_nodes_at_depth(%s,%s);""", [depth,architecture])
    rows = c.fetchall()
    c.close()
    """
    we must close the cursor, otherwise we will get 
    ProgrammingError: (2014, "Commands out of sync; you can't run this command now")
    """
    return getHits(db,rows)

def getNodeIdFromDomains(db, domains):
    """
    Basically try to find all pfamseq ids whose architecture contain all of given domains, 
    link them back to the tree of life.
    """
    whereClause = 'WHERE ' + ' AND '.join(['arch.architecture_acc LIKE \'%'+d+'%\'' for d in domains])
    sql = """SELECT GROUP_CONCAT(a.seq_ids) as seq_ids, tp.tree_node_id AS id FROM (
            -- select all sequences that match the architecture pattern
            SELECT GROUP_CONCAT(seq.pfamseq_id) as seq_ids, \
                          seq.ncbi_taxid \
                   FROM   pfam.architecture arch, pfam.pfamseq seq 
                   """+whereClause+""" \
                   AND arch.auto_architecture = seq.auto_architecture
                   GROUP BY seq.ncbi_taxid
            ) AS a, tree_of_life.pfam_tree as tp WHERE a.ncbi_taxid = tp.proteome_tax_id 
        GROUP BY tp.tree_node_id;
    """
    c = db.cursor(MySQLdb.cursors.DictCursor)
    setGroupConcatMax = """SET group_concat_max_len = 32768;"""
    c.execute(setGroupConcatMax);
    c.execute(sql)
    rows = c.fetchall()
    return getHits(db,rows)

def checkStrFormat(regex,strs):
    for s in strs: 
        if not re.search(regex,s):
            return False
    return True

def getPfamIdFromAcc(db, acc):
    sql = """
    SELECT pfamA_id FROM pfam.pfamA WHERE pfamA_acc = %s;
    """
    c = db.cursor(MySQLdb.cursors.DictCursor)
    c.execute(sql, [acc])
    rows = c.fetchall()
    if (len(rows)==0):
        return 'Not a valid accession'
    return rows[0]['pfamA_id']

"""
@param archOrDomains: [<string> ...]
where string is either pfam acc "PF00123" or '*'
"""
def getQueryUsed(db, archOrDomains):
    searchPhrases = [getPfamIdFromAcc(db,x) if isPfamAcc(x) else 'any domain' for x in archOrDomains]
    return ', '.join(searchPhrases)

def isPfamAcc(text):
    return bool(re.match('^PF\d{5}$',text))

"""
returns a json string out 
"""
def getTree(tableName):
    assert(len(tableName) > 0)
    return tree_exporter.export_tree(getDb(), table=tableName)
#============================================================


def make_error_response(msg='Default error, unknown reasone',code=500):
    return make_response(jsonify({'error': msg}), code)

def bad_request(msg='Bad request'):
    return make_error_response(msg=msg,code=400)

def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()
    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers
            h['Content-Type'] = 'Application/JSON'
            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

@app.route('/phydo/pfamDomain/autocomplete', methods=['OPTIONS'])
@crossdomain(origin='*',headers=['content-type','accept'])
def allowCORSAutocomplete():
    return True

@app.route('/phydo/pfamDomain/autocomplete', methods=['GET'])
@crossdomain(origin='*',headers=['content-type','accept'])
def pfamDomainAutocomplete():
    db = getDb()
    phrase = request.args.get('query')
    if not phrase:
        return jsonify([])
    if not phrase or len(phrase) == 0:
        return jsonify([])
    elif re.search('%',phrase):
        return make_error_response(msg='invalid autocomplete phrase, percent sign is not allowed',code=400)
    else: 
        c = db.cursor(MySQLdb.cursors.DictCursor)
        sql = """SELECT description, pfamA_acc, pfamA_id FROM pfam.pfamA WHERE LOWER(description) 
        LIKE %s OR LOWER(pfamA_id) LIKE %s LIMIT 10;"""
        searchPhrase = phrase.lower() + '%' # so that it matches anything starting with `phrase`
        c.execute(sql,[searchPhrase, searchPhrase])
        rows = c.fetchall()
        return jsonify(rows)

@app.route('/phydo/taxonomy/autocomplete', methods=['OPTIONS'])
@crossdomain(origin='*',headers=['content-type','accept'])
def allowTaxonomyCORSAutocomplete():
    return True


@app.route('/phydo/taxonomy/autocomplete', methods=['GET'])
@crossdomain(origin='*',headers=['content-type','accept'])
def taxonomyAutocomplete():
    db = getDb(database="pfam")
    phrase = request.args.get('query')
    if not phrase or len(phrase) == 0:
        return jsonify([])
    elif re.search('%',phrase):
        return make_error_response(msg='invalid autocomplete phrase, percent sign is not allowed',code=400)
    else:
        c = db.cursor(MySQLdb.cursors.DictCursor)
        sql = """SELECT pfam.get_genus(ncbi_taxid) as taxId, species FROM taxonomy WHERE LOWER(species)
        LIKE %s AND pfam.get_genus(ncbi_taxid)!=1 LIMIT 10;"""
        searchPhrase = phrase.lower() + '%' # so that it matches anything starting with `phrase`
        c.execute(sql,[searchPhrase])
        rows = c.fetchall()
        return jsonify(rows)

@app.route('/phydo/treeNodes/by/taxIds', methods=['OPTIONS'])
@crossdomain(origin='*', headers=['content-type', 'accept'])
def allowCORSTaxIds():
    return True


@app.route('/phydo/treeNodes/by/taxIds', methods=['POST'])
@crossdomain(origin='*')
def queryByTaxIds():
    _db = getDb()
    if not request.json:
        abort(400)
        return
    taxids = request.json
    if not checkStrFormat('\d+',taxids):
        return make_error_response(msg='invalid tax ids', code=400)

    return jsonify(getNodeIdFromTaxIds(_db,taxids))

@app.route('/phydo/treeNodes/by/architecture', methods=['OPTIONS'])
@crossdomain(origin='*', headers=['content-type', 'accept'])
def allowCORSArchitecture():
    return True


@app.route('/phydo/treeNodes/by/architecture', methods=['POST'])
@crossdomain(origin='*')
def queryByArchitecture():
    """
    expected input: 
    {
        architecture: [<string>... ] where <string> is either a 
            pfam accession e.g. PF00123, or '*'
    }
    """
    db = getDb()
    if not request.json:
        return abort(400)
    if 'architecture' not in request.json:
        return bad_request(msg='architecture not given')
    archPattern = request.json['architecture']
    if len(archPattern) == 0:
        return bad_request(msg='empty architecture')
    for a in archPattern: 
        if (a != '*' and not isPfamAcc(a) ):
            return bad_request(msg='%s is not a valid phrase, * or pfam accession expected'%a)
    # add a space to after every pfam accession, to make sure they are space separated, disallow any 
    # space in the beginning or end
    # also change * to %
    # construct a suitable pattern for the SQL `LIKE` phrase, e.g. '%PF00123 PF00652'
    archPatternQuery = (''.join([a+' ' if a.startswith('PF') else '%' for a in archPattern])).strip()
    hits = getNodeIdFromArchitecture(db,999,archPatternQuery) # search all nodes in tree, giving maximum depth 999
    result = {
        'queryUsed': getQueryUsed(db, archPattern),
        'hits': hits,
    }
    return jsonify(result)

@app.route('/phydo/treeNodes/by/domains', methods=['OPTIONS'])
@crossdomain(origin='*', headers=['content-type', 'accept'])
def allowCORSDomain():
    return True


@app.route('/phydo/treeNodes/by/domains', methods=['POST'])
@crossdomain(origin='*')
def queryByDomain():
    """
    expected input: 
    {
        domains: [<string>... ] where <string> is a 
            pfam accession e.g. PF00123
    }
    """
    db = getDb()
    if not request.json:
        return abort(400)
    if 'domains' not in request.json:
        return bad_request(msg='domains not given')
    domains = request.json['domains']
    if len(domains) == 0:
        return bad_request(msg='empty domains')
    for d in domains: 
        if not isPfamAcc(d):
            return bad_request(msg='%s is not a valid pfam accession'%d)
    hits = getNodeIdFromDomains(db,domains)
    result = {
        'queryUsed': getQueryUsed(db, domains),
        'hits': hits,
    }
    return jsonify(result)


@app.route('/phydo/manualLabel', methods=['OPTIONS'])
@crossdomain(origin='*', headers=['content-type', 'accept'])
def allowCORSManualLabelling():
    return True

def isInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

@app.route('/phydo/manualLabel', methods=['POST'])
@crossdomain(origin='*', headers=['content-type', 'accept'])
def annotateNode():
    db = getDb();
    if not request.json:
        return abort(400)
    manualLabel = request.json
    """
    expect the json to be
    {
        id: <number>,
        manualRank: <string>,
        manualTaxId: <string>,
        manualTaxonName: optional <string>,
        curator: optional <string>,
    }
    """
    if 'id' not in manualLabel or 'manualRank' not in manualLabel or 'manualTaxId' not in manualLabel:
        return bad_request(msg='expects id, manualRank, manualTaxId to be in the json data')
    id = manualLabel['id']
    manualRank = manualLabel['manualRank']
    manualTaxId = manualLabel['manualTaxId']

    if (manualRank != 'phylum' and manualRank != 'class') or not isInt(manualTaxId) or int(manualTaxId) < 1: 
        return bad_request(msg='expected manualRank to be one of "phylum" or "class" and a valid positive integer for manualTaxId')
    manualTaxonName = manualLabel['manualTaxonName'] if 'manualTaxonName' in manualLabel else None
    curator = manualLabel['curator'] if 'curator' in manualLabel else None
    c = db.cursor()
    sql = """INSERT INTO tree_of_life.manual_label (id, manual_rank, manual_tax_id,
        manual_taxon_name, curator) 
        VALUES(%s, %s, %s, %s, %s) 
        ON DUPLICATE KEY UPDATE 
        manual_rank=VALUES(manual_rank), manual_tax_id=VALUES(manual_tax_id), 
        manual_taxon_name=VALUES(manual_taxon_name), curator=VALUES(curator);
    """
    c.execute(sql, [id, manualRank, manualTaxId, manualTaxonName, curator])
    db.commit()
    return jsonify(True)



@app.route('/phydo/unprunedTree', methods=['OPTIONS'])
@crossdomain(origin='*', headers=['content-type', 'accept'])
def allowCORSUnprunedtree():
    return True

@app.route('/phydo/unprunedTree', methods=['GET'])
@crossdomain(origin='*', headers=['content-type', 'accept'])
def getUnprunedTree():
    return getTree('tree_of_life.node')


@app.route('/phydo/prunedTree', methods=['OPTIONS'])
@crossdomain(origin='*', headers=['content-type', 'accept'])
def allowCORSPrunedtree():
    return True

@app.route('/phydo/prunedTree', methods=['GET'])
@crossdomain(origin='*', headers=['content-type', 'accept'])
def getPrunedTree():
    return getTree('tree_of_life.pruned_node')

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, threaded=True)

