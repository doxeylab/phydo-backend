

#installation

need ubuntu 14.04
```
bash install.sh
```


#starting server (well you need to have database set up first...)

The database setup script is not here, so check back later

```
./flask/bin/python ./app.py
```

#Setting up Database

Add a `config.py` file that looks like the following in the project root:

```
username=''
password=''
host=''
database='tree_of_life' # note that tree_of_life should be the default database name
```


