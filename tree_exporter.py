import os
import json
import sys 



"""
Generally when you add a new field to the json represetation of the tree, 
you have to change 
1. the database
2. the SQL query below
3. TreeNode __init__ function
4. TreeNode toDict function
5. row_to_node function 

definitely looking for ways to refactor this
"""

def get_rows(db, table = None):
    sql = """SELECT n.id, n.parent_id, n.comment,n.length, n.name,
    IFNULL(ml.manual_rank, (SELECT rank FROM pfam.taxonomy WHERE ncbi_taxid = n.tax_id)) AS rank,
    IFNULL(ml.manual_tax_id, n.tax_id) AS tax_id,
    IF(ml.manual_tax_id IS NOT NULL, 
        (SELECT level FROM pfam.taxonomy WHERE ncbi_taxid = ml.manual_tax_id),
        (SELECT level FROM pfam.taxonomy WHERE ncbi_taxid = n.tax_id)) AS level,
    p.counts, ng.genus_id,n.is_leaf, 
    n.phylum_tax_id, n.class_tax_id, 
    (SELECT level FROM pfam.taxonomy WHERE ncbi_taxid = n.phylum_tax_id) AS phylum_level,
    (SELECT level FROM pfam.taxonomy WHERE ncbi_taxid = n.class_tax_id) AS class_level
    FROM """+table+""" n 
    LEFT JOIN tree_of_life.node_genus ng ON n.id = ng.node_id
    LEFT JOIN tree_of_life.pfam_tree_counts p ON ng.genus_id = p.genus_id
    LEFT JOIN pfam.taxonomy t ON n.tax_id = t.ncbi_taxid
    LEFT JOIN tree_of_life.manual_label ml ON n.id = ml.id
    ;"""
    db.query(sql)
    result = db.store_result()
    rows = result.fetch_row(maxrows=0,how=1)
    return rows


class TreeNode:
    def __init__(self,length=None,parentId=None,id=None,taxId=None,comment=None,
        name=None,counts=None,genusId=None,children = None,
        isLeaf=None,level=None, rank=None ,phylumLevel = None, phylumTaxId = None, 
        classLevel = None, classTaxId = None):
        self.length = length 
        self.parentId = parentId
        self.id = id
        self.taxId = taxId
        self.comment = comment
        self.name = name
        self.counts = counts
        self.genusId = genusId
        self.children = children if children else list()
        self.isLeaf = isLeaf
        self.level = level # this is the level name corresponding to labeled tax id
        self.rank = rank 
        self.phylumLevel = phylumLevel
        self.phylumTaxId = phylumTaxId
        self.classLevel = classLevel
        self.classTaxId =classTaxId 
    def to_dict(self):
        return {
            'length': self.length ,
            'parentId': self.parentId,
            'id': self.id,
            'taxId': self.taxId,
            'comment': self.comment,
            'name': self.name,
            'counts': self.counts,
            'genusId': self.genusId,
            'children': [c.to_dict() for c in self.children],
            'isLeaf': self.isLeaf,
            'level': self.level,
            'rank': self.rank,
            'classLevel':self.classLevel,
            'classTaxId':self.classTaxId,
            'phylumTaxId':self.phylumTaxId,
            'phylumLevel':self.phylumLevel,
        }
    def count_nodes(self):
        return 1+sum([c.count_nodes() for c in self.children])


def get_tree_json(root):
    return json.dumps(root.to_dict())

def row_to_node(row):
    return TreeNode(
        length=row['length'],
        parentId=row['parent_id'],
        id=row['id'],
        taxId=row['tax_id'],
        comment=row['comment'],
        name=row['name'],
        counts=row['counts'],
        genusId=row['genus_id'],
        isLeaf = None if row['is_leaf'] is None else int(row['is_leaf']) == 1,
        level = row['level'],
        rank = row['rank'],
        phylumLevel = row['phylum_level'],
        classLevel = row['class_level'],
        phylumTaxId = row['phylum_tax_id'],
        classTaxId = row['class_tax_id'],
        )

def _link_tree(nodes,n): 
    # multiple fail safe checks
    if (not n.id) or (not n.parentId) or (n.id == n.parentId) or (n in nodes[n.parentId].children):
        return 
    parent = nodes[n.parentId]
    parent.children.append(n)
    _link_tree(nodes,parent)


def link_tree (nodes): 
    tree_nodes = nodes.values()
    for n in tree_nodes:
        _link_tree(nodes,n)

def _get_tree(rows):
    nodes = {r['id']:row_to_node(r) for r in rows }
    link_tree(nodes)
    return (nodes[1],nodes) # we know that the root node has id 1


def _load_tree_db(db,table=None):
    rows = get_rows(db,table=table)
    root,nodes = _get_tree(rows)
    return (root,nodes)

"""
Given a database connection `db` and a table representing the tree
e.g. `tree_of_life.pruned_node` or `tree_of_life.node`
Export the tree json
"""
def export_tree(db,table=None):
    (root,_) = _load_tree_db(db,table=table)
    return get_tree_json(root)



